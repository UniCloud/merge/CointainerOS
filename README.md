# CointainerOS

Desktops in Containers

Inspire:  
https://community.kde.org/Neon/Docker

# Connecting Xhost
This is the "Secret Sauce":  
xserver code

```
xhost +
docker run -ti -v /tmp/.X11-unix:/tmp/.X11-unix -v ~/src:/home/neon/src -e DISPLAY=:0 kdeneon/plasma:dev-unstable bash
```